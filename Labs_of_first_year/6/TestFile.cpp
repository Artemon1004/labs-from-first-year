#include <iostream>
#include <cstdio>
using namespace std;


struct statbook
{
	int simbols;
	int words;
	int puncts;
	int digits;
	float avglen;
	char popsim;
};
//������� ��� ������ ���������� �� �����
void print(statbook st);
{
	cout << endl;
	cout << "���������� ������ �����:" << endl
	<< "���������� ��������: " << st.simbols << endl
	<< "���������� ����: " << st.words << endl
	<< "���������� ������ ����������: " << st.puncts << endl
	<< "���������� ����: " << st.digits << endl
	<< "������� ����� �����: " << st.avglen << endl
	<< "����� ���������� ������: " << st.popsim << endl;
}

void main()
{
	setlocale(LC_ALL, "Russian");
	statbook st;
	FILE *fp;
	char namef[30];

	st.avglen = st.digits = st.popsim = st.puncts = st.simbols = st.words = 0;

	cout << "������������! ����������,������� ��� ����� � �����������: ";
	cin >> namef;

	fp = fopen(namef, "rt");

	if (fp == NULL)
	{
		perror("FILE error");
		cout << "Check name of file..." << endl;
		exit(1);
	}

	int ch, arr[256] = { 0 };
	int sum = 0, inWord = 0; //��� ����������� �������

	while ((ch = fgetc(fp)) != EOF) //ch - ��� ���������� �������
	{
		st.simbols++;
		if (isdigit(ch) != 0) //isdigit - �������� �� �����
			st.digits++;
		if (ispunct(ch) != 0) //ispunct - �������� �� ���� ����������
			st.puncts++;
		arr[ch]++;

		if (ch != ' ' && ch != '\n' && ch != '\t' && inWord == 0) 
		{
			inWord = 1;
			st.words++;
			sum++;
		}
		else if (ch != ' ' && ch != '\n' && ch != '\t' && inWord == 1)
				sum++;
		if ((ch == ' ' || ch == '\n' || ch == '\t') && inWord == 1)
			inWord = 0;
	}//while

	st.avglen = sum / st.words;

	int i, place, max = 0;

	for (i = 0; i != 256; i++)
	{
		if (arr[i] > max)
		{
			max = arr[i];
			place = i;
		}//if
	}//for

	st.popsim = place;

	print(st);
}