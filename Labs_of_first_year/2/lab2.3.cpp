#include <stdio.h>
#include <string.h>

/*
* ���������, ������������� � ������������ ������ � 
* ��������� �� ����� ������� ������������� �������� ��� ��������� ������. 
* ������� ������������ �������� � ������� �������� �� �����.
*/


int main() {
	char str[128] = { 0 };
	int counts[128] = { 0 };
	int i;

	puts("Hello!! Enter your string: ");
	fgets(str, 129, stdin);
	str[strlen(str) - 1] = 0;
	putchar('\n');

	for (i = 0; i != strlen(str); i++) 
		counts[str[i]]++;

	for (i = 0; i != 128; i++) {
		if (counts[i] != 0)
			printf("'%c' - %d times\n", i, counts[i]);
	}//for

	putchar('\n');

	return 0;
}//main