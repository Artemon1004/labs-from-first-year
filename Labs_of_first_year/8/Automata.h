#pragma once
#include <string>
#include <stdio.h>

using namespace std;

enum STATES { OFF, WAIT, ACCEPT, CHECK, COOK };

typedef struct
{
	char drink[51];
	unsigned int price;
} MENU;
class Automata
{
private:
	MENU *menu;
	int cash;
	STATES state;
	int choose;
	int numberofdrinks;
public:
	Automata();
	void on();
	void off();
	void coin(int);
	void printMenu();
	STATES getState();
	void printState();
	void choice(int);
	bool check();
	void cancel();
	void cook();
	void finish();
};