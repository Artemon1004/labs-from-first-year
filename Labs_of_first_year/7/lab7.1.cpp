#include <stdio.h>
#include "circle.h"
#include <iostream>
#include <iomanip>
using namespace std;

#define RAD 6378100 //������ ������

void dialog()
{
	cout<<("Program: \"The Earth and a rope\".")
	<<endl<<endl
	<<("Description of functions:")
	<<endl
	<<("\"SetRad\":  enters the radius and considers circle ference and area;")<<endl	
	<<("\"GetRad\":  return radius of circle")<<endl
	<<("\"SetFer\":  enters the ference and considers circle radius and area;")<<endl
	<<("\"GetFer\":  return ference of circle")<<endl
	<<("\"SetArea\": enters the area and considers circle ference and radius;")<<endl
	<<("\"GetArea\": return area of circle")<<endl
	<<endl<<endl
	<<("Constants:")
	<<endl
	<<("PI = 3.14159265358979323846")<<endl
	<<("Radius of Earth = 6378100 meters")
	<<endl<<endl;
}

void main()
{
	dialog();

	circle cir;
	double distance, len1;

	cir.SetRad(RAD); //�������� ����� ���������� � ������� ������
	len1 = cir.GetFer();
	cir.SetFer(len1 + 1); //��������� � ������� ��� 1 ����, �������� ����� ������ � �������
	distance = cir.GetRad() - RAD; //��������� = ����� ������ - ������ �����

	printf("Distance between the rope and the Earth: %f meters\n\n", distance);
}