#include <stdio.h>
#include "circle.h"
#include <iostream>
using namespace std;

#define RADPOOL 3 //������ ��������
#define WIDTH 1 //������ �������
#define CEMENT 1000 //���� �� 1 ��.� ������
#define FENCE 2000 //���� �� 1 ���� ������

void dialog()
{
	cout<<("Program: \"The Earth and a rope\".")<<endl
	<<endl<<endl
	<<("Description of functions:")
	<<endl
	<<("\"SetRad\": enters the radius and considers circle ference and area;")<<endl
	<<("\"GetRad\": return radius of circle")<<endl
	<<("\"SetFer\": enters the ference and considers circle radius and area;")<<endl
	<<("\"GetFer\": return ference of circle")<<endl
	<<("\"SetArea\": enters the area and considers circle ference and radius;")<<endl
	<<("\"GetArea\": return area of circle.")<<endl
	<<endl<<endl
	<<("Constants:")
	<<endl
	<<("PI = 3.14159265358979323846;")<<endl
	<<("Radius of pool = 3 meters;")<<endl
	<<("Width of footpath = 1 meters;")<<endl
	<<("Price of cement = 1000 rub;")<<endl
	<<("Price of fence = 2000 rub;")<<endl
	<<endl<<endl;
}

void main()
{
	dialog();

	circle cir;
	double prFoot, prFence, s1, s2;

	cir.SetRad(RADPOOL); //�������� ������� ��������
	s1 = cir.GetArea();
	cir.SetRad(WIDTH + RADPOOL); //�������� ������� � ����� �������� �����
	s2 = cir.GetArea();
	prFoot = (s2 - s1) * CEMENT;

	prFence = cir.GetFer() * FENCE;

	cout<<"The cost of footpath "<< prFoot<<endl
	<<"The cost of fence "<< prFence<<endl
	<<"Sum "<<prFoot + prFence<<endl;
}