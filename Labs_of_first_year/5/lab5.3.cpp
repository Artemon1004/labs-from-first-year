#include <stdio.h>
#include <string.h>

struct BOOK *openFile(int *N);

struct BOOK
{
	char author[32];
	char title[64];
	int year;
};

int main()
{
	struct BOOK *arr;
	int count = 0;
	char temp[32];

	arr = openFile(&count);

	for (int i = count - 1; i >= 0; i--)
		for (int j = 0; j < i; j++)
			if (strcmp(arr[j].author, arr[j + 1].author) > 0)
			{
				strcpy(temp, arr[j].author);
				strcpy(arr[j].author, arr[j + 1].author);
				strcpy(arr[j + 1].author, temp);
			}

	puts("Sort by author:");
	putchar('\n');

	for (int i = 0; i <= count; i++)
	{
		printf("Author: %sTitle: %sYear: %d", arr[i].author, arr[i].title, arr[i].year);
		putchar('\n');
		putchar('\n');
	}

	return 0;
}