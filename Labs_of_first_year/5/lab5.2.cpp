#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

struct BOOK *openFile(int *N);

struct BOOK
{
	char author[32];
	char title[64];
	int year;
};

int main()
{
	struct BOOK *arr;
	int count = 0, i;
	int max = 0, min = 3000, maxBook, minBook;
	
	arr = openFile(&count);

	for (i = 0; i <= count; i++)
	{
		if (arr[i].year < min)
		{
			min = arr[i].year;
			minBook = i;
		}
		if (arr[i].year > max)
		{
			max = arr[i].year;
			maxBook = i;
		}
	}

	puts("The OLDets book:");
	printf("Title: %sAuthor: %sYear: %d\n\n", arr[minBook].title, arr[minBook].author, arr[minBook].year);

	puts("The NEWest book:");
	printf("Title: %sAuthor: %sYear: %d\n\n", arr[maxBook].title, arr[maxBook].author, arr[maxBook].year);

	return 0;
}