#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct BOOK
{
	char author[32];
	char title[64];
	int year;
};

struct BOOK *openFile(int *N)
{
	FILE *fp = fopen("b.txt", "rt");
	struct BOOK *arr;
	char buf[64] = { 0 };
	int i = 0, count = *N;

	arr = (struct BOOK*)calloc(5, sizeof(struct BOOK));

	if (fp == NULL)
	{
		perror("File error!!!");
		exit(1);
	}
	
	while (fgets(buf, 64, fp)) //!= NULL
	{
		if (buf[0] == '\n')
		{
			i = 0;
			count++;
			if (count % 5 == 0)
				arr = (struct BOOK*)realloc(arr, sizeof(struct BOOK) * (count + 5));			
		}//if buf
		else
			switch (i)
			{
			case 0: strcpy(arr[count].author, buf); i++; break;
			case 1: strcpy(arr[count].title, buf); i++; break;
			case 2: arr[count].year = atoi(buf); i++;
			}//switch
	}//while

	*N = count;

	return arr;
}//openFile